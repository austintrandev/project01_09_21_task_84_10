package com.shoppizza365.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.shoppizza365.model.*;
import com.shoppizza365.repository.*;

@RestController
public class COrderDetailController {
	@Autowired
	private IOrderDetailRepository orderDetailRepository;

	@Autowired
	private IOrderRepository orderRepository;
	
	@Autowired
	private IProductRepository productRepository;

	@CrossOrigin
	@GetMapping("/orderdetails")
	public ResponseEntity<List<COrderDetail>> getAllOrderDetails() {
		try {
			List<COrderDetail> pOrderDetails = new ArrayList<COrderDetail>();

			orderDetailRepository.findAll().forEach(pOrderDetails::add);

			return new ResponseEntity<>(pOrderDetails, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/orderdetail/{id}")
	public ResponseEntity<COrderDetail> getOrderDetailById(@PathVariable("id") long id) {
		Optional<COrderDetail> orderDetailData = orderDetailRepository.findById(id);
		if (orderDetailData.isPresent()) {
			return new ResponseEntity<>(orderDetailData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@GetMapping("/order/{order_id}/orderdetail")
	public List<COrderDetail> getOrderDetailByOrderId(@PathVariable Long order_id) {
		if (orderRepository.findById(order_id).isPresent())
			return orderRepository.findById(order_id).get().getOrderDetail();
		else
			return null;
	}
	
	@CrossOrigin
	@GetMapping("/product/{product_id}/orderdetail")
	public List<COrderDetail> getOrderDetailByProductId(@PathVariable Long product_id) {
		if (productRepository.findById(product_id).isPresent())
			return productRepository.findById(product_id).get().getOrderDetail();
		else
			return null;
	}

	@CrossOrigin
	@PostMapping("/orderdetail/create/{order_id}/{product_id}")
	public ResponseEntity<Object> createOrderDetail(@PathVariable("order_id") Long order_id,@PathVariable("product_id") Long product_id
			,@RequestBody COrderDetail pOrderDetail) {
		Optional<COrder> orderData = orderRepository.findById(order_id);
		Optional<CProduct> productData = productRepository.findById(product_id);
		if (orderData.isPresent() && productData.isPresent()) {
			try {
				COrderDetail newOrderDetail = new COrderDetail();
				newOrderDetail.setPriceEach(pOrderDetail.getPriceEach());
				newOrderDetail.setQuantityOrder(pOrderDetail.getQuantityOrder());
				newOrderDetail.setOrder(orderData.get());
				newOrderDetail.setProduct(productData.get());
				COrderDetail savedOrderDetail = orderDetailRepository.save(newOrderDetail);
				return new ResponseEntity<>(savedOrderDetail, HttpStatus.CREATED);
			} catch (Exception e) {
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@PutMapping("/orderdetail/update/{id}")
	public ResponseEntity<Object> updateOrderDetailById(@PathVariable("id") long id,
			@RequestBody COrderDetail pOrderDetail) {
		Optional<COrderDetail> orderDetailData = orderDetailRepository.findById(id);
		if (orderDetailData.isPresent()) {
			COrderDetail newOrderDetail = orderDetailData.get();
			newOrderDetail.setPriceEach(pOrderDetail.getPriceEach());
			newOrderDetail.setQuantityOrder(pOrderDetail.getQuantityOrder());
			COrderDetail savedOrderDetail = orderDetailRepository.save(newOrderDetail);
			try {
				return new ResponseEntity<>(savedOrderDetail, HttpStatus.OK);
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity()
						.body("Failed to Update specified OrderDetail:" + e.getCause().getCause().getMessage());
			}

		} else {
			// return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			return ResponseEntity.badRequest().body("Failed to get specified OrderDetail: " + id + "  for update.");
		}
	}

	@CrossOrigin
	@DeleteMapping("/orderdetail/delete/{id}")
	public ResponseEntity<COrderDetail> deleteOrderDetailById(@PathVariable("id") long id) {
		try {
			orderDetailRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/orderdetail/quantity_order/{quantity}")
	public ResponseEntity<List<COrderDetail>> findOrderDetailByQuantityOrder(@PathVariable long quantity) {
		try {
			List<COrderDetail> vOrderDetail = orderDetailRepository.findOrderDetailByQuantityOrder(quantity);
			return new ResponseEntity<>(vOrderDetail, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/orderdetail/quantity_order_pageable/{quantity}/{start}/{end}")
	public ResponseEntity<List<COrderDetail>> findOrderDetailByQuantityOrderPageable(@PathVariable long quantity,
			@PathVariable int start, @PathVariable int end) {
		try {
			List<COrderDetail> vOrderDetail = orderDetailRepository.findOrderDetailByQauntityOrderPageable(quantity,
					PageRequest.of(start, end));
			return new ResponseEntity<>(vOrderDetail, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/orderdetail/quantity_order_desc/{status}/{start}/{end}")
	public ResponseEntity<List<COrderDetail>> findOrderDetailByQuantityOrderDESC(@PathVariable long quantity,
			@PathVariable int start, @PathVariable int end) {
		try {
			List<COrderDetail> vOrderDetail = orderDetailRepository.findOrderDetailByQuantityOrderDESC(quantity,
					PageRequest.of(start, end));
			return new ResponseEntity<>(vOrderDetail, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@PutMapping("/orderdetail/update_for_quantity/{quantity}")
	public int updateQuantityOrder(@PathVariable("quantity") long quantity) {
		return orderDetailRepository.updateQuantityOrder(quantity);
	}
}
