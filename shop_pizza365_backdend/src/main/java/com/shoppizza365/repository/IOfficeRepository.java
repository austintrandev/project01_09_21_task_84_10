package com.shoppizza365.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.shoppizza365.model.*;

public interface IOfficeRepository extends JpaRepository<COffice, Long> {
	@Query(value = "SELECT * FROM offices  WHERE city like %:name% OR state like %:name%", nativeQuery = true)
	List<COffice> findOfficeByCityOrState(@Param("name") String name);
	
	@Query(value = "SELECT * FROM offices  WHERE city like %:name% OR state like %:name%", nativeQuery = true)
	List<COffice> findOfficeByCityOrStatePageable(@Param("name") String name,Pageable pageable);
	
	@Query(value = "SELECT * FROM offices  WHERE city like %:name% OR state like %:name% ORDER BY city DESC", nativeQuery = true)
	List<COffice> findOfficeByCityOrStateDESC(@Param("name") String name,Pageable pageable);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE offices SET country = :country WHERE country IS NULL", nativeQuery = true)
	int updateCountry(@Param("country") String country);
}
